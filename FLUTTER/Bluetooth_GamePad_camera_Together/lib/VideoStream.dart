import 'dart:async';
import 'dart:ui';
import 'dart:convert' show utf8;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:gesture_zoom_box/gesture_zoom_box.dart';
import 'package:intl/intl.dart';

import 'package:sbr_phone_flutter/MainPage.dart';

import 'package:web_socket_channel/web_socket_channel.dart';

import 'package:control_pad/control_pad.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:control_pad/models/gestures.dart';

class VideoStream extends StatefulWidget {
  final BluetoothDevice server;

  final WebSocketChannel channel;

  VideoStream({Key key, @required this.channel, this.server}) : super(key: key);

  @override
  _VideoStream createState() => _VideoStream();
}

class _VideoStream extends State<VideoStream> {
  BluetoothConnection connection;

  bool isConnecting = true;
  bool get isConnected => connection != null && connection.isConnected;

  bool isDisconnecting = false;

  final double videoWidth = 320;
  final double videoHeight = 420;

  bool isLandscape;
  String _timeString;

  var _globalKey = new GlobalKey();

  Timer _timer;
  bool isRecording;

  int frameNum;

  @override
  void initState() {
    super.initState();
    isLandscape = false;
    isRecording = false;

    BluetoothConnection.toAddress(widget.server.address).then((_connection) {
      print('Connected to the device');
      connection = _connection;
      setState(() {
        isConnecting = false;
        isDisconnecting = false;
      });
    }).catchError((error) {
      print('Cannot connect, exception occured');
      print(error);
    });

    frameNum = 0;
  }

  @override
  void dispose() {
    widget.channel.sink.close();
    _timer.cancel();
    // Avoid memory leak (`setState` after dispose) and disconnect
    if (isConnected) {
      isDisconnecting = true;
      connection.dispose();
      connection = null;
    }
    super.dispose();
  }

  writeData(String data) {
    if (data == null) return;

    connection.output.add(utf8.encode(data + "\r\n"));
  }

  @override
  Widget build(BuildContext context) {
    JoystickDirectionCallback onDirectionChanged(
        double degrees, double distance) {
      String datatest = "";
      /*String data =
          "Degree : ${degrees.toStringAsFixed(2)}, distance : ${distance.toStringAsFixed(2)}";*/
      String data = "55555555";

      if (distance > 0.5) {
        if ((degrees > 340.00) || (degrees < 20.00)) {
          data = "11111111";
        } else if ((degrees > 70.00) && (degrees < 110.00)) {
          data = "22222222";
        } else if ((degrees > 160.00) && (degrees < 200.00)) {
          data = "33333333";
        } else if ((degrees > 250.00) && (degrees < 290.00)) {
          data = "44444444";
        } else {
          data = "87654321";
        }

        print(data);
        writeData(data);
        datatest =
            "Degree : ${degrees.toStringAsFixed(2)}, distance : ${distance.toStringAsFixed(2)}";
        print(datatest);
      }
    }

    PadButtonPressedCallback padBUttonPressedCallback(
        int buttonIndex, Gestures gesture) {
      String data = "55555555";
      if (buttonIndex == 0) {
        data = "66666666";
      } else if (buttonIndex == 1) {
        data = "77777777";
      } else if (buttonIndex == 2) {
        data = "88888888";
      } else if (buttonIndex == 3) {
        data = "99999999";
      } else {
        data = "12345678";
      }

      print(data);
      writeData(data);
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('GamePad CAM'),
          centerTitle: true,
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Container(
                width: videoWidth,
                height: videoHeight,
                color: Colors.white,
                child: StreamBuilder(
                  stream: widget.channel.stream,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      Future.delayed(Duration(milliseconds: 100)).then((_) {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => MainPage()));
                      });
                    }
                    if (!snapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                      );
                    } else {
                      return Row(
                        children: <Widget>[
                          Transform.rotate(
                            angle: 3.14 / 180 * 90,
                            alignment: Alignment.center,
                            child: //[
                                Stack(
                              children: <Widget>[
                                RepaintBoundary(
                                  key: _globalKey,
                                  child: GestureZoomBox(
                                    maxScale: 5.0,
                                    doubleTapScale: 2.0,
                                    duration: Duration(milliseconds: 200),
                                    child: Image.memory(
                                      snapshot.data,
                                      gaplessPlayback: true,
                                      /*width: videoWidth,
                                  height: videoHeight,*/
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            // ],
                          ),
                        ],
                      );
                    }
                  },
                ),
              ),
              Divider(),
              Container(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                    JoystickView(
                      onDirectionChanged: onDirectionChanged,
                      interval: Duration(seconds: 1),
                    ),
                    PadButtonsView(
                      padButtonPressedCallback: padBUttonPressedCallback,
                    ),
                  ]))
            ],
          ),
        ));
  }
}
