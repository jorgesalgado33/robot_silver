/**
 * \file doxygen_template.h
 * \author The Author
 * \date 20 Jun 2016
 * \todo TODO for this file.
 *
 * \brief A brief description of the file
 *
 * A more detailed description can be placed here.
 * Multiple lines of description have to start with *.
 *
 * If you want to end a command, add an empty line (starting with *).
 *
 * Inside of a comment block, the user can use doxygen commands like "brief" etc..
 * A list of all available commands can be found at \see https://www.stack.nl/~dimitri/doxygen/manual/commands.html .
 *
 * You can reference to entries in the documentation by using #. Example: #myEnum_t.
 * 
 * 
 * Changes
 * 13.04.2020: Doc was created
 * 
 *
 */

#ifndef SERVOS_MANAGER_H
#define SERVOS_MANAGER_H

//####################### Defines/Macros
 /**
 * \brief a brief description of what the define is representing
 *
 * If needed, a more detailed description can be given below */
#define CMD_SERVO_MOVE		        0x03
#define CMD_ACTION_GROUP_RUN		0x06	 /**< \brief by putting a < next to the comment-start. The documentation referes to the left instead to the next line. */
#define CMD_ACTION_STOP             0x07
#define CMD_ACTION_GROUP_COMPLETE   0x08 
#define CMD_ACTION_SPEED            0x0B    //11u
#define CMD_GET_BATTERY_VOLTAGE     0x0F
#define CMD_MULT_SERVO_UNLOAD       0x14
#define CMD_MULT_SERVO_POS_READ     0x15

#define HEADER_SERVOS_COM           0x55


/**************************************/
/* DEFINITION GROUP NUMBER				*/
/**************************************/
#define GROUP_NUMBER0				0x00
#define GROUP_NUMBER1				GROUP_NUMBER0+1
#define GROUP_NUMBER2				GROUP_NUMBER1+1
#define GROUP_NUMBER3				GROUP_NUMBER2+1
#define GROUP_NUMBER4				GROUP_NUMBER3+1
#define GROUP_NUMBER5				GROUP_NUMBER4+1
#define GROUP_NUMBER6				GROUP_NUMBER5+1
#define GROUP_NUMBER7				GROUP_NUMBER6+1
#define GROUP_NUMBER8				GROUP_NUMBER7+1
#define GROUP_NUMBER9				GROUP_NUMBER8+1

#define WALKING_FOWARD				GROUP_NUMBER1	
#define WALKING_BACK				GROUP_NUMBER3
#define SLIDE_RIGHT					GROUP_NUMBER4
#define SLIDE_LEFT					GROUP_NUMBER2
#define ARM_ACTION					GROUP_NUMBER7
#define POSITION_STABLE				GROUP_NUMBER8
/****************SERVOS DEFINES **************/
#define SERVO_NUMBER				0x1A

#define HEAD_SERVO					SERVO_NUMBER

#define LIMITEHEADRIGHT				1080
#define LIMITEHEADLEFT				1860

//#define HEAD_TO_RIGH
//#define HEADE_To_LEFT

//####################### Enumerations
/**
 * \brief Enumerations. Use brief, otherwise the index won't have a brief explanation.
 *
 * Detailed explaination of the enumeration.
 */

//####################### Structures
/**
 * \brief The purpose as well as the members of a structure have to be documented.
 *
 * Make clear what the structure is used for and what is the purpose of the members.
 *
 */



//####################### class
/**
 * \brief The purpose as well as the members and methos of a class have to be documented.
 *
 * Make clear what the class is used for and what is the purpose of the members and methos.
 */
class ServosManager{


private:
	
	void GroupAction(char u8NumberGroup, int u16Repetions);

    void ServoAction(char u8NumberGroup, int u16Timming, int u16Position);

	int ReadServos(char u8NumberServo);

public:

	int PositionHead;
	/**
	 * \brief Example showing how to document a function with Doxygen.
	 *
	 * Description of what the function does. This part may refer to the parameters
	 * of the function, like \p param1 or \p param2. A word of code can also be
	 * inserted like \c this which is equivalent to <tt>this</tt> and can be useful
	 * to say that the function returns a \c void or an \c int.
	 * We can also include text verbatim,
	 * \verbatim like this\endverbatim
	 * Sometimes it is also convenient to include an example of usage:
	 * \code
	 * uint32_t myFunction(uint32_t param1, myStruct_t param2);
	 * printf("something...\n");
	 * \endcode
	 * Or,
	 * \code{.py}
	 * pyval = python_func(arg1, arg2)
	 * print pyval
	 * \endcode
	 * when the language is not the one used in the current source file.
	 * By the way, <b>this is how you write bold text</b> or,
	 * if it is just one word, then you can just do \b this.
	 * \param param1 Description of the first parameter of the function.
	 * \param param2 The second one, which follows \p param1.
	 * \return Describe what the function returns.
	 * \see myStruct_t
	 * \see http://website/
	 * \note Something to note.
	 * \warning Warnings for this function.
	 * \todo A TODO for this function
	 */
	ServosManager();

    ~ServosManager();

	void Run(char ButtonPressed);
	
	void Listenning();
	
};



#endif /* SERVOS_MANAGER_H */
