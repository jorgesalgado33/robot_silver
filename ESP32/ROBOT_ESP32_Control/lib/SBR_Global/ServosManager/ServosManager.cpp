/**
 * \file doxygen_template.h
 * \author The Author
 * \date 20 Jun 2016
 * \todo TODO for this file.
 *
 * \brief A brief description of the file
 *
 * A more detailed description can be placed here.
 * Multiple lines of description have to start with *.
 *
 * If you want to end a command, add an empty line (starting with *).
 *
 * Inside of a comment block, the user can use doxygen commands like "brief" etc..
 * A list of all available commands can be found at \see https://www.stack.nl/~dimitri/doxygen/manual/commands.html .
 *
 * You can reference to entries in the documentation by using #. Example: #myEnum_t.
 * 
 * 
 * Changes
 * 13.04.2020: Doc was created
 * 
 *
 */

#include "ServosManager.h"
#include "HardwareSerial.h"

ServosManager::ServosManager(){
    // Serial Communicate 
    Serial2.begin(9600);
    /*Initialize*/
    PositionHead = 1480;
}

ServosManager::~ServosManager(){
    
}

void ServosManager::GroupAction(char u8NumberGroup, int u16Repetions){
    char RepetionsLSB;
    char RepetionsMSB;

    RepetionsLSB = (char)u16Repetions;
    RepetionsMSB = ((0xFF00)&u16Repetions)>>8;
    Serial2.write(HEADER_SERVOS_COM);
    Serial2.write(HEADER_SERVOS_COM);
    Serial2.write(0x5);
    Serial2.write(CMD_ACTION_GROUP_RUN);
    Serial2.write(u8NumberGroup);//foward
    Serial2.write(RepetionsLSB);
    Serial2.write(RepetionsMSB);

}

void ServosManager::ServoAction(char u8NumberServo, int u16Timming, int u16Position){
    char TimmingLSB;
    char TimmingMSB;
    char PositionLSB;
    char PositionMSB;

    TimmingLSB =(char)u16Timming;
    TimmingMSB = ((0xFF00)&u16Timming)>>8;

    PositionLSB =(char)u16Position;
    PositionMSB = ((0xFF00)&u16Position)>>8;

    Serial2.write(HEADER_SERVOS_COM);
    Serial2.write(HEADER_SERVOS_COM);
    Serial2.write(0x8);
    Serial2.write(CMD_SERVO_MOVE);
    Serial2.write(u8NumberServo);        //buton A  ----> moving head  26 => 1A
    Serial2.write(TimmingLSB);        //Time value 1000ms => 0x3E8
    Serial2.write(TimmingMSB);        //
    Serial2.write(u8NumberServo);        //ID servo
    Serial2.write(PositionLSB);         //Position  1480 => 0x5C8
    Serial2.write(PositionMSB);

}

int ServosManager::ReadServos(char u8NumberServo){

    Serial2.write(HEADER_SERVOS_COM);
    Serial2.write(HEADER_SERVOS_COM);
    Serial2.write(0x3);
    Serial2.write(CMD_MULT_SERVO_POS_READ);
    Serial2.write(0x01);                 //quantity of read servos
    //Serial2.write(0x01);                 //quantity of read servos
    //Serial2.write(u8NumberServo);        //Time value 1000ms => 0x1A3E8
    //Serial2.write(u8NumberServo+1);        //Time value 1000ms => 0x1A3E8
    //Serial.printf("Id Servo:%x\n",u8NumberServo);

    return 1480;
}

void ServosManager::Listenning(){
    if(Serial2.available()){
            int header = Serial2.read();
            Serial.printf("Header:%x\n",header);

            int header2 = Serial2.read();
            Serial.printf("Header:%x\n",header2);

            int lenght = Serial2.read();
            Serial.printf("lenght:%x\n",lenght);

            int command = Serial2.read();
            Serial.printf("command:%x\n",command);

            int Par = Serial2.read();
            Serial.printf("Par:%x\n",Par);

            int Par2 = Serial2.read();
            Serial.printf("Par2:%x\n",Par2);

            int Par3 = Serial2.read();
            Serial.printf("Par2:%x\n",Par3);
    }
}

void ServosManager::Run(char ButtonPressed){
    
    switch (ButtonPressed)
    {
        case '1':
            GroupAction(WALKING_FOWARD,0x01);               
            break;
        case '2':
            GroupAction(SLIDE_LEFT,0x1);
            break;
        case '3':
            GroupAction(WALKING_BACK,0x1);
            break;
        case '4':
            GroupAction(SLIDE_RIGHT,0x1);
            break;
        case '6':
            if(PositionHead > LIMITEHEADRIGHT){
                PositionHead-=50;
            }else{
                PositionHead = LIMITEHEADRIGHT;
            }
                
            ServoAction(HEAD_SERVO,500,PositionHead);
            break;
        case '7':
            GroupAction(ARM_ACTION,0x1); //buton B ---->  Position ESPADA
            break;
        case '8':
            if(PositionHead < LIMITEHEADLEFT){
                PositionHead+=50;
            }else{
                PositionHead = LIMITEHEADLEFT;
            }
            
            ServoAction(HEAD_SERVO,500,PositionHead);
            break;
        case '9':
            GroupAction(POSITION_STABLE,0x1); //buton D position Head
            break;    
        default:
            break;
    }
}